
public class Fraction {
	private int num;
	private int den;
	private static final Fraction UN = new Fraction(1);
    private static final Fraction ZERO = new Fraction(0,1);
	
	public Fraction(int num, int den){
		this.num = num;
		this.den = den;
	}
	
	public Fraction(int num){
		this.num = num;
		this.den = 1;
	}
	
	public Fraction(){
		this.num = 0;
		this.den = 1;
	}
	
	public int getNum() {
		return this.num;
	}
	
	public int getDen() {
		return this.den;
	}
	
	public double constult() {
		return this.num/this.den;
	}
	
	@Override
	public String toString() {
		return this.num + "/" + this.den;
	}
}
